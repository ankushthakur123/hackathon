//
//  ViewController.swift
//  SafetyApp
//
//  Created by Guneet Singh on 2018-07-06.
//  Copyright © 2018 Guneet Singh. All rights reserved.
//

import UIKit
import MessageUI
import MapKit
import CoreLocation

class ViewController: UIViewController, MFMessageComposeViewControllerDelegate, CLLocationManagerDelegate {

//    let manager = CLLocationManager()
//    var locationData: CLLocation!;
//    let distanceSpan: Double = 500
    @IBAction func callButton(_ sender: Any) {
        if let url = URL(string: "tel://(911)") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else
            {
                print("This device does not have call functionality")
            }
        }
    }
    
    let locationManager = CLLocationManager()
    var previousPoint: CLLocation? = nil
    var totalMovementDistance = CLLocationDistance(0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        //manager.startUpdatingLocation()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "launch.png")!)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
   
        self.dismiss(animated: true, completion: nil)
    }
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorization status changed to \(status.rawValue)")
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        //mapView.showsUserLocation = true
        default:
            locationManager.stopUpdatingLocation()
            //mapView.showsUserLocation = false
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.last
        {
            let latitudeString = String(format: "%g\u{00B0}",newLocation.coordinate.latitude)
            //                latitudeLabel.text = latitudeString
            //                latitudeLabel.text = latitudeString
            print(latitudeString)
            
            let longitudeString = String(format: "%g\u{00B0}",
                                         newLocation.coordinate.longitude)
            //                longitudeLabel.text = longitudeString
            print(longitudeString)
        }
    }
    @IBAction func sendSMS(_ sender: Any) {
        
        if MFMessageComposeViewController.canSendText()
        {
            let controller = MFMessageComposeViewController()
            controller.body = "Hey there!! ;)"
            controller.recipients = ["1111111111"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            print("Test..!!")
            
            }
    }
   
    }


