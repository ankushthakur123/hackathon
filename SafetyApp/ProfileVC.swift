//
//  ProfileVC.swift
//  SafetyApp
//
//  Created by Guneet Singh on 2018-07-06.
//  Copyright © 2018 Guneet Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    override func viewDidLoad() {
        super.viewDidLoad()
        imgPick.layer.cornerRadius = imgPick.frame.size.width/2
        imgPick.clipsToBounds = true
        
       /* let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.persistentContainer.viewContext
        
       // let entity = NSEntityDescription.entity(forEntityName: "Users", in: context!)
        
       // let newUser = NSManagedObject(entity: entity!, insertInto: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        //request.predicate = NSPredicate(format: "age = %@", "12")
        
        request.returnsObjectsAsFaults = false
        
        
        
        do {
            
            let result = try context?.fetch(request)
            
            for data in result?.count as! [NSManagedObject] {
                
                self.name = data.value(forKey: "username") as! UITextField
                 self.phoneNumber = data.value(forKey: "username") as! UITextField
                //print;(data.value(forKey: "username") as! String)
                
            }
            
            
            
        } catch {
            
            
            
            print("Failed")
            
        }
        */
        
    }
    
    @IBOutlet weak var imgPick: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBAction func saveInfo(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let context = appDelegate?.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context!)
        
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        let name = self.name.text!
        
        let phone = self.phoneNumber.text!
        
        newUser.setValue(name, forKey: "username")
        
        newUser.setValue(phone, forKey: "phone")
        
        //newUser.setValue("12", forKey: "age")
        
        
        
        do {
            
            
            
            try context?.save()
            
            
            
        } catch {
            
            
            
            print("Failed saving")
            
        }
        
        
        
      /*  let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        //request.predicate = NSPredicate(format: "age = %@", "12")
        
        request.returnsObjectsAsFaults = false
        
        
        
        do {
            
            let result = try context?.fetch(request)
            
            for data in result as! [NSManagedObject] {
                
                print(data.value(forKey: "username") as! String)
                 print(data.value(forKey: "phone") as! String)
                
            }
            
            
            
        } catch {
            
            
            
            print("Failed")
            
        }*/
        (sender as! UIButton).isEnabled = false 
        
    }
    @IBAction func chooseImage(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
            self.present(imagePickerController, animated: true, completion: nil)
            }
                else
                {
                    print("Camera not available")
                }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction) in }))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgPick.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
